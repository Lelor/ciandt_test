from abc import ABC


class BasePage(ABC):
    def __init__(self, driver):
        self.driver = driver
        self.search_field_selector = '#inpHeaderSearch'
        self.search_field_btn_selector = '#btnHeaderSearch'
    
    def _query_selector(self, selector):
        return self.driver.find_element_by_css_selector(selector)
    
    def _query_selector_all(self, selector):
        return self.driver.find_elements_by_css_selector(selector)

    @property
    def search_field(self):
        return self._query_selector(self.search_field_selector)

    @property
    def search_field_btn(self):
        return self._query_selector(self.search_field_btn_selector)

    def search_product(self, text):
        self.search_field.send_keys(text)
        self.search_field_btn.click()
