from .base import BasePage

from selenium.common.exceptions import NoSuchElementException

from helper import poll

# There should be an abstract page object such as BasePage exclusively for the cart section
class Cart(BasePage):
    def __init__(self, driver):
        super(Cart, self).__init__(driver)
        self.cart_items_selector = '.BasketTable > .BasketItem'
        self.cart_item_title_selector = '.BasketItemProduct-info-title > p'
        self.cart_item_amount_selector = '.BasketItemProduct-quantity-dropdown'
        self.proceed_btn_selector = '.BasketContinue-button'
    
    @property
    def cart_items(self):
        return self._query_selector_all(self.cart_items_selector)

    @property
    def proceed_btn(self):
        return self._query_selector(self.proceed_btn_selector)
    
    def _get_cart_item_title(self, el):
        return el.find_element_by_css_selector(self.cart_item_title_selector).text

    @poll(exception=NoSuchElementException)
    def _filter_cart_item(self, title):
        try:
            return next(filter(lambda el: title in self._get_cart_item_title(el),
                               self.cart_items))
        except StopIteration:
            raise NoSuchElementException(
                f'No item with the title {title} was found on the cart')
    
    def get_item_amount(self, title):
        item = self._filter_cart_item(title)
        dropdown_el = item.find_element_by_css_selector(self.cart_item_amount_selector)
        return int(dropdown_el.get_attribute('value'))
