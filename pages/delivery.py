from .base import BasePage
from helper import poll


class DeliveryPage(BasePage):
    def __init__(self, driver):
        super(DeliveryPage, self).__init__(driver)
        self.proceed_btn_selector = '.DeliveryPage-continue'

    @property
    def proceed_btn(self):
        return self._query_selector(self.proceed_btn_selector)

    # TODO: implement methods to interact with the delivery options
