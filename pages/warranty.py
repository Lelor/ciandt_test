from .base import BasePage


class WarrantyProposal(BasePage):
    def __init__(self, driver):
        super(WarrantyProposal, self).__init__(driver)
        self.continue_btn_selector = '.btn-buy-warranty'
    
    @property
    def proceed_button(self):
        return self._query_selector(self.continue_btn_selector)

    # TODO: methods to navigate through the warranty options
