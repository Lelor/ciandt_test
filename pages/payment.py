from selenium.webdriver.support.ui import Select

from .base import BasePage


class Payment(BasePage):
    def __init__(self, driver):
        super(Payment, self).__init__(driver)
        self.card_number_input_selector = 'input[name="number"]'
        self.owner_name_input_selector = 'input[name="fullName"]'
        self.month_input_selector = '#expirationMonth'
        self.year_input_selector = '#expirationYear'
        self.security_code_input_selector = 'input[name="CVC"]'
        self.conclude_btn_selector = '.continueButton'

    @property
    def card_number_input(self):
        return self._query_selector(self.card_number_input_selector)

    @property
    def owner_name_input(self):
        return self._query_selector(self.owner_name_input_selector)

    @property
    def month_input(self):
        return self._query_selector(self.month_input_selector)

    @property
    def year_input(self):
        return self._query_selector(self.year_input_selector)
        
    @property
    def security_code_input(self):
        return self._query_selector(self.security_code_input_selector)

    @property
    def conclude_btn(self):
        return self._query_selector(self.conclude_btn_selector)

    def set_month(self, value):
        Select(self.month_input).select_by_value(value)
    
    def set_year(self, value):
        Select(self.month_input_selector).select_by_value(value)
