from .base import BasePage


class Login(BasePage):
    def __init__(self, driver):
        super(Login, self).__init__(driver)
        self.email_input_selector = '#LoginBox-form input[name="login"]'
        self.password_input_selector = '#LoginBox-form input[name="password"]'
        self.login_btn_selector = '#LoginBox-form .LoginBox-form-continue'

    @property
    def login_email_input(self):
        return self._query_selector(self.email_input_selector)
    
    @property
    def password_input(self):
        return self._query_selector(self.password_input_selector)

    @property
    def login_btn(self):
        return self._query_selector(self.login_btn_selector)
