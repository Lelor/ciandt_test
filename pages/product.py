from .base import BasePage


class Product(BasePage):
    def __init__(self, driver):
        super(Product, self).__init__(driver)
        self.title_selector = '.header-product__title'
        self.add_to_cart_btn_selector = '.button__buy'
    
    @property
    def product_title(self):
        return self._query_selector(self.title_selector).text

    @property
    def add_to_cart_button(self):
        return self._query_selector(self.add_to_cart_btn_selector)
