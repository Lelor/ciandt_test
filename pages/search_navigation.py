from itertools import cycle
from selenium.common.exceptions import NoSuchElementException

from .base import BasePage
from helper import poll


class SearchNavigation(BasePage):
    def __init__(self, driver):
        super(SearchNavigation, self).__init__(driver)
        self.product_list_selector = '.neemu-products-container > li'
        self.product_title_selector = '.nm-product-name'
    
    @property
    def products_list(self):
        return self._query_selector_all(self.product_list_selector)

    def _get_product_title(self, element):
        return element.find_element_by_css_selector(self.product_title_selector).text

    @poll(exception=NoSuchElementException)
    def get_product(self, text):
        result = filter(lambda el: text in self._get_product_title(el),
                        self.products_list)
        try:
            return next(result)
        except StopIteration:
            raise NoSuchElementException(
                msg=f'No product with the title {text} was found')
