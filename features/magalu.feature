Funcionalidade: E-commerce Magalu

  Contexto:
    Dado que seja feito o acesso a página "https://www.magazineluiza.com.br/"

  Cenario: Comprar produto pesquisado via cartão de crédito
    Dado que seja pesquisado o produto "cadeira"
    Quando clicar no produto "Cadeira de Escritório Giratória PRE-002 - Nell"
    Entao deve ser redirecionado a pagina do produto "Cadeira de Escritório Giratória PRE-002 - Nell"
    Quando adicionar o produto a sacola
    Entao deve ser ofertada a opção de garantia estendida
    Quando continuar a compra sem garantia estendida
    Entao devera redirecionar para a sacola
    E deve conter "1" item "Cadeira de Escritório Giratória PRE-002 - Nell" na sacola
    Quando prosseguir com a compra dos itens da sacola
    E efetuar o login
    E manter a entrega padrao
    E realizar o pagamento via cartao de credito
    Entao a compra deve ser concluida
