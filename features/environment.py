from selenium import webdriver


def before_all(context):
    context.driver = webdriver.Chrome("chromedriver")
    context.email = context.config.userdata['email']
    context.password = context.config.userdata['password']


def after_all(context):
    context.driver.close()
