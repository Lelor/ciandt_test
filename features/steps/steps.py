from behave import given, when, then

from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as ec

from pages.cart import Cart
from pages.delivery import DeliveryPage
from pages.home import Home
from pages.login import Login
from pages.payment import Payment
from pages.product import Product
from pages.search_navigation import SearchNavigation
from pages.warranty import WarrantyProposal


@given('que seja feito o acesso a página "{page}"')
def get_page_step(context, page):
    context.driver.get(page)


@given('que seja pesquisado o produto "{text}"')
def search_bar_filter_step(context, text):
    page = Home(context.driver)
    page.search_product(text)


@when('clicar no produto "{product}"')
def click_on_product_step(context, product):
    page = SearchNavigation(context.driver)
    page.get_product(product).click()


@then('deve ser redirecionado a pagina do produto "{product}"')
def assert_product_redirect_step(context, product):
    page = Product(context.driver)
    assert page.product_title == product,\
        f'{page.product_title} is not {product}'


@when('adicionar o produto a sacola')
def add_to_cart_step(context):
    page = Product(context.driver)
    page.add_to_cart_button.click()


@then('deve ser ofertada a opção de garantia estendida')
def assert_extended_warranty_step(context):
    assert 'produto/garantia-plus' in context.driver.current_url,\
        'Current url does not match with the warranty offer'


@when('continuar a compra sem garantia estendida')
def proceed_adding_to_cart_step(context):
    page = WarrantyProposal(context.driver)
    page.proceed_button.click()
    wait = WebDriverWait(context.driver, 10)
    wait.until(ec.title_contains("Sacola de compras"))


@then('devera redirecionar para a sacola')
def assert_redirect_to_cart_step(context):
    assert context.driver.current_url.startswith('https://sacola.magazineluiza.com.br'),\
        'Current url does not match with the cart route'


@then('deve conter "{amount:d}" item "{product}" na sacola')
def assert_items_in_cart(context, amount, product):
    page = Cart(context.driver)
    found_value = page.get_item_amount(product)
    assert found_value == amount,\
        f'expected: {amount}, found: {found_value}'


@when('prosseguir com a compra dos itens da sacola')
def proceed_to_buy_step(context):
    page = Cart(context.driver)
    page.proceed_btn.click()


@when('efetuar o login')
def do_login_step(context):
    page = Login(context.driver)
    page.login_email_input.send_keys(context.email)
    page.password_input.send_keys(context.password)
    page.login_btn.click()


@when('manter a entrega padrao')
def proceed_to_payment_step(context):
    page = DeliveryPage(context.driver)
    page.proceed_btn.click()


@when('realizar o pagamento via cartao de credito')
def conclude_payment_step(context):
    page = Payment(context.driver)
    page.card_number_input.send_keys('9999999999999999999')
    page.owner_name_input.send_keys('ci&test')
    page.set_month('4')
    page.set_year('2022')
    page.security_code_input.send_keys('999')
    # page.conclude_btn.click()

@then('a compra deve ser concluida')
def assert_credit_card_payment_step(context):
    ...