from functools import wraps
from time import sleep


def poll(attempts=5, interval=.3, exception=Exception):
    """
    Tries to execute the function over again when an exception is raised
    according to params.
    """
    @wraps
    def _inner(func):
        def decorator(*args, **kwargs):
            err = None
            for attempt in range(attempts):
                try:
                    return func(*args, **kwargs)
                except exception as e:
                    err = e
                    sleep(interval)
            raise err
        return decorator
    return _inner
        